Feature: Memory
    In order to ensure safe operations
    As a critical node
    I want an appropriate amount of free Memory
    @Memory @Free
    Scenario: Free Memory
        When inspecting the node memory
        Then at least 500MB is free for use
    @Memory @PageEntries
    Scenario: System Table Page Entries
        When inspecting the node system table
        Then at least 5000 page entries are available
