Given 'one or more system volumes' {
    $Script:Volumes = Get-Volume `
    | Where-Object -FilterScript {
        $_.DriveType -eq 'Fixed' -and $_.FileSystemLabel -ne 'System Reserved'
    }
    $Script:SystemDriveLetter = $env:SystemDrive.Substring(0, 1)
}

Then 'all volumes are reporting as healthy' {
    ForEach ($Volume in $Script:Volumes) {
        $Volume.HealthStatus | Should -Be 'Healthy'
    }
}

When 'inspecting the system drive' {
    $Script:SystemDrive = $Script:Volumes `
    | Where-Object -FilterScript {$_.DriveLetter -eq $Script:SystemDriveLetter} 
}

When 'inspecting all non-system drives' {
    $Script:OtherDrives = $Script:Volumes `
    | Where-Object -FilterScript {$_.DriveLetter -ne $Script:SystemDriveLetter} 
}

Then 'the system drive has at least (?<MinimumExpectedFreeSpace>\S*) free' {
    Param ($MinimumExpectedFreeSpace)
    Switch -Wildcard ($MinimumExpectedFreeSpace) {
        "*MB" {
            $ExpectedFreeSpace = [int]$MinimumExpectedFreeSpace.Replace('MB',$null)
            ($Script:SystemDrive.SizeRemaining / 1MB) -ge $ExpectedFreeSpace | Should -Be $true
        }

        "*GB" {
            $ExpectedFreeSpace = [int]$MinimumExpectedFreeSpace.Replace('GB',$null)
            ($Script:SystemDrive.SizeRemaining / 1GB) -ge $ExpectedFreeSpace | Should -Be $true
        }
    }
}

Then 'the system drive has at least (?<MinimumExpectedFreeSpace>\S*) free space' {
    Param ($MinimumExpectedFreeSpace)
    $SystemDriveFreePercentage = $Script:SystemDrive.SizeRemaining / $Script:SystemDrive.Size
    $MinimumExpectedFreePercentage = [int]($MinimumExpectedFreeSpace.replace('%',$null)) / 100
    $SystemDriveFreePercentage -ge $MinimumExpectedFreePercentage | Should -Be $true
}

Then 'those drives each have at least (?<MinimumExpectedFreeSpace>\S*) free' {
    Param ($MinimumExpectedFreeSpace)
    Switch -Wildcard ($MinimumExpectedFreeSpace) {
        "*MB" {
            $ExpectedFreeSpace = [int]$MinimumExpectedFreeSpace.Replace('MB',$null)
            $Unit = 1MB
        }

        "*GB" {
            $ExpectedFreeSpace = [int]$MinimumExpectedFreeSpace.Replace('GB',$null)
            $Unit = 1GB
        }
    }
    ForEach ($Drive in $Script:OtherDrives) {
        $($Drive.SizeRemaining / $Unit) -ge $ExpectedFreeSpace | Should -Be $true
    }
}

Then 'those drives each have at least (?<MinimumExpectedFreeSpace>\S*) free space' {
    Param ($MinimumExpectedFreeSpace)
    $MinimumExpectedFreePercentage = [int]($MinimumExpectedFreeSpace.replace('%',$null)) / 100
    ForEach ($Drive in $Script:OtherDrives) {
        $DriveFreePercentage = $Drive.SizeRemaining / $Drive.Size
        $DriveFreePercentage -ge $MinimumExpectedFreePercentage | Should -Be $true
    }
}

When 'inspecting the node memory' {
     $Script:FreeMemory = Get-CimInstance -Query 'SELECT FreePhysicalMemory FROM Win32_OperatingSystem' `
     | Select-Object -ExpandProperty FreePhysicalMemory
}

Then 'at least (?<ExpectedAvailableMemory>\S*) is free for use' {
    Param ($ExpectedAvailableMemory)
    Switch -Wildcard ($ExpectedAvailableMemory) {
        "*MB" {
            ($Script:FreeMemory / 1KB) -ge [int]$ExpectedAvailableMemory.Replace('MB',$null) `
            | Should -Be $true
        }

        "*GB" {
            ($Script:FreeMemory / 1MB) -ge [int]$ExpectedAvailableMemory.Replace('GB',$null) `
            | Should -Be $true
        }
    }
}

When 'inspecting the node system table' {
    $Script:FreePageEntries = Get-Counter -Counter '\Memory\Free System Page Table Entries'`
    | Select-Object -ExpandProperty CounterSamples `
    | Select-Object -First 1 -ExpandProperty CookedValue
}

Then 'at least (?<ExpectedAvailablePages>\S*) page entries are available' {
    Param ($ExpectedAvailablePages)
    $Script:FreePageEntries | Should -BeGreaterThan $ExpectedAvailablePages
}

When 'inspecting the network adapters' {
    $Script:Adapters = Get-NetAdapter -Physical
}

Then 'all network adapters report as up' {
    ForEach ($Adapter in $Script:Adapters) {
        $Adapter.Status | Should -Be 'Up'
    }
}

Given 'we have a service called (?<Service>\S*)' {
    Param ($ServiceName)
    $Script:ServiceObject = Get-Service -Name $ServiceName
}

Then 'the state of (?<ServiceName>\S*) should be (?<Status>\S*)' {
    Param ($ServiceName,$Status)
    $Script:ServiceObject.Name   | Should -Be $ServiceName
    $Script:ServiceObject.Status | Should -Be $Status
}