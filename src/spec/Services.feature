Feature: Operating System Services
    In order to ensure safe operations
    As a critical node
    I want all OS services to be running
    @Services
    Scenario Outline: services are running
        Given we have a service called <Service>
        Then the state of <Service> should be <State>

        Scenarios: OS Services
        | Service           | State   |
        | DHCP              | Running |
        | DNSCache          | Running |
        | Eventlog          | Running |
        | PlugPlay          | Running |
        | RpcSs             | Running |
        | lanmanserver      | Running |
        | LmHosts           | Running |
        | Lanmanworkstation | Running |
        | MpsSvc            | Running |
        | WinRM             | Running |