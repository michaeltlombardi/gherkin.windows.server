Feature: Network
    In order to ensure safe operations
    As a critical node
    I want all network adapters are up
    @Network
    Scenario: All network adapters are up
        When inspecting the network adapters
        Then all network adapters report as up