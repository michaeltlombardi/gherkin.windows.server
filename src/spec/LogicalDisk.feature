Feature: Logical Disks
    In order to ensure safe operations
    As a critical node
    I want healthy logical disks with appropriate free space

Background: The node has system volumes
    Given one or more system volumes

    @Disks @Health
    Scenario: Logical Disk Health
        Then all volumes are reporting as healthy
    @Disks @SystemDrive @Capacity
    Scenario: System Drive Capacity
        When inspecting the system drive
        Then the system drive has at least 10MB free
        And  the system drive has at least 10% free space
    @Disks @Capacity
    Scenario: Non-System Drives Capacity
        When inspecting all non-system drives
        Then those drives each have at least 10MB free
        And  those drives each have at least 10% free space