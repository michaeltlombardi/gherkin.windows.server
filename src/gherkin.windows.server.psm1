Get-Module -ListAvailable -Name Pester `
| Select-Object -ExpandProperty Version `
| ForEach-Object -Process {
    If ($_ -ge [version]'4.0.0') {
        $Continue = $true
    }
}
If (([string]::IsNullOrEmpty($Continue)) -and ($Continue -ne $true)) {
    Throw 'Unable to load module; no version of Pester is available which supports Invoke-Gherkin'
}

Function Test-Infrastructure {
    [CmdletBinding(DefaultParameterSetName='Default')]
    param(
        [Parameter(ParameterSetName='RetestFailed', Mandatory=$true)]
        [switch]
        ${FailedLast},

        [Parameter(Position=1)]
        [Alias('Name','TestName')]
        [string[]]
        ${ScenarioName},

        [Parameter(Position=2)]
        [switch]
        ${EnableExit},

        [Parameter(Position=4)]
        [Alias('Tags')]
        [string[]]
        ${Tag},

        [string[]]
        ${ExcludeTag},

        [switch]
        ${Strict},

        [string]
        ${OutputFile},

        [ValidateSet('NUnitXml')]
        [string]
        ${OutputFormat},

        [switch]
        ${Quiet},

        [System.Object]
        ${PesterOption},

        [Pester.OutputTypes]
        ${Show},

        [switch]
        ${PassThru})

    begin {
        try {
            $outBuffer = $null
            if ($PSBoundParameters.TryGetValue('OutBuffer', [ref]$outBuffer)) {
                $PSBoundParameters['OutBuffer'] = 1
            }
            $wrappedCmd = $ExecutionContext.InvokeCommand.GetCommand('Invoke-Gherkin', [System.Management.Automation.CommandTypes]::Function)
            $scriptCmd = {& $wrappedCmd -Path $PSScriptRoot\spec @PSBoundParameters }
            $steppablePipeline = $scriptCmd.GetSteppablePipeline()
            $steppablePipeline.Begin($PSCmdlet)
        } catch {
            throw
        }
    }

    process {
        try {
            $steppablePipeline.Process($_)
        } catch {
            throw
        }
    }

    end {
        try {
            $steppablePipeline.End()
        } catch {
            throw
        }
    }

    <#
        .ForwardHelpTargetName Invoke-Gherkin
        .ForwardHelpCategory Function
    #>
}

Export-ModuleMember -Function *