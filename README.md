# Gherkin.Windows.Server
_OVF, but pickled._

This module is an example of how we can write infrastructure verification tests using the gherkin syntax.
The tests themselves are forked from [DevBlackOps's](https://twitter.com/devblackops) [OVF.Windows.Server](https://github.com/devblackops/OVF.Windows.Server) project.

These tests are written to be human-readable, even by people who don't have an operations background.
The hope is that tests like these can be turned over to partners in the organization who rely on the infrastructure you support but who don't themselves know everything that should be checked.

## Installation
<!--
You can install this module from the PowerShell Gallery:

```powershell
Install-Module -Repository PSGallery -Name Gherkin.Windows.Server
Import-Module Gherkin.Windows.Server
```

Alternatively, you can download the zip of this project, expand it, and copy into your PSModulePath.
```powershell
Invoke-WebRequest -Uri '' -UseBasicParsing -OutFile $ENV:Temp\Gherkin.Windows.Server.zip
Expand-Archive -Path $ENV:\Temp\Gherkin.Windows.Server.zip -DestinationPath 'C:\Program Files\WindowsPowerShell\Modules\Gherkin.Windows.Server.zip'
Remove-Item -Path $ENV:\Temp\Gherkin.Windows.Server.zip
Import-Module Gherkin.Windows.Server
```



The final option is to clone this project locally and import the module from there.
-->
You can test this module by cloning the project and importing the module locally.
```powershell
git clone 'https://gitlab.com/michaeltlombardi/gherkin.windows.server.git'
Import-Module Pester
Import-Module .\Gherkin.Windows.Server\Gherkin.Windows.Server.psd1
```

## Run the Tests
I've created a helper function for the test suite, `Test-Infrastructure`, which wraps the `Invoke-Gherkin` command.
Play around with it to see your own results.

```powershell
Test-Infrastructure
Test-Infrastructure -Tag Memory
Test-Infrastructure -ExcludeTag Services
Test-Infrastructure -OutputFormat NUnitXml -OutputFile .\test-results.xml
Test-Infrastructure -Passthru -Show None
```

## Next Steps
I'm going to noodle on writing infrastructure and compliance tests using gherkin now that the possibility of it has been proved out.

What do you think? Are these tests more readable for non-engineers and sysadmin?
Does that even matter?
What would you like to see from this type of project?